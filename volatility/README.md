# "An advanced memory forensics framework"

## Supported tags and respective `Dockerfile` links

* `latest` ([*volatility/Dockerfile*](TBA))

## Project homepage

https://github.com/volatilityfoundation/volatility
