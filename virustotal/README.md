# "Analyze suspicious files and URLs to detect types of malware"

## Supported tags and respective `Dockerfile` links

* `latest` 
([*virustotal/Dockerfile*](https://gitlab.com/CinCan/Tools/blob/master/pipelines/dockerfiles/virustotal/Dockerfile))

## Project homepage

https://github.com/VirusTotal
