# Upload a PDF to www.pdfexaminer.com/pdfapi.php and get results

## Supported tags and respective `Dockerfile` links
* `latest` 
([*pdfexaminer/Dockerfile*](https://gitlab.com/CinCan/dockerfiles/blob/master/pdfexaminer/Dockerfile))

## Usage

`$ docker run -v /samples:/samples cincan/pdfexaminer /samples/input/sample.pdf  
[OUTPUT FORMAT(default JSON)]`

## Project homepage

https://github.com/mwtracker/pdfexaminer_tools
