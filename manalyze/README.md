# "A static analyzer for PE executables"

## Supported tags and respective `Dockerfile` links

* `latest` 
([*manalyze/Dockerfile*](https://gitlab.com/CinCan/dockerfiles/tree/master/manalyze))

## Project homepage

https://github.com/JusticeRage/Manalyze
